# CI Example
CI - CD Example

# Requirements

## Flask app
pip install Flask pytest

## Production Server
apt install docker-compose

# Steps

1. Create public repo.
2. Create web folder and app.py.
3. Create test folder and `conftest.py` and `test_api.py`.
4. Create `.coveragerc`.
5. Create `deploy.sh`.
6. Create `Dockerfile`.
7. Create `docker-compose.yml` file.
8. Create `.gitlab-ci.yml` file.
9. Deploy app on production server: `docker-compose -f docker-compose.yml up --build`
10. Change app.py and update test accordingly.
11. Push changes and check for Gitlab Pipeline.

# Source

https://medium.com/@huseinzolkepli/how-to-ci-cd-flask-app-using-gitlab-ci-4297017acda1