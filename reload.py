import os
import subprocess as sp

from flask import request, Flask

app = Flask(__name__)


@app.route('/reload')
def reload():
    if request.args.get('password') == 'udg123':
        try:
            output = sp.getoutput("git pull")
            return output
        except:
            return 'fail'
    else:
        return 'wrong password'
